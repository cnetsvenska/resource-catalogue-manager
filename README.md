## Resource Catalogue Manager

In   order to easy and make the access and creation of the OGC Sensorthing API resources in controlled way a Resource Catalogue Manager is used. The Resource Catalogue Manager makes sure that all the created Things, Datastreams et c. are using a common meta data model. The Resource Catalogue manager provides also a unified API for searching. The Resource Catalogue Manager provides an interactive swagger page that describes the service and also provides a Swagger interface to test the API using a web browser. 

![SWAGGER](https://bitbucket.org/cnetsvenska/resource-catalogue-manager/raw/master/images/swagger.PNG)

## Getting started

The Resource Catalogue Manager is developed in C# using Aspnet Core 2.1. The preferred development environment is Visual Studio 2019.

The Resource Catalogue Manager is deployed as a linux based Docker container. 

NB! The addresses for the GOST and MQTT service needs to defined as ENV variables when deploying the Docker container


## Affiliation
![GOEASY](https://bitbucket.org/cnetsvenska/nb-iotudpserver/raw/master/images/GOEASY_HD_Logo2.png)
This work is supported by the European Commission through the [GOEASY H2020 PROJECT](https://goeasyproject.eu/) under grant agreement No 776261.